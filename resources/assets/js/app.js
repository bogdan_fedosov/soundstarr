
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('croppie');
require('video.js/dist/video.min.js');




$(function () {
    $(".not-pressing").click(function (e) {
        e.preventDefault();
    });
    
    $(".play-it").click(function (e) {
        if (!$("audio", this).get(0).paused) {
            $("audio", this).get(0).pause();
            $("button", this).removeClass("playing");
        } else {
            $("audio", this).get(0).play();
            $("button", this).addClass("playing");

        }

        e.preventDefault();
        e.stopPropagation();
    });



    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

   
});



