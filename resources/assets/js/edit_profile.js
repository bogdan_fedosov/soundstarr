/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    var $uploadCrop = $('#avatar-file').croppie({
        enableExif: true,
        showZoomer: false,
        viewport: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        boundary: {
            width: 200,
            height: 200
        }
    });

    $('#file-selector').on('change', function () {
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar-file').addClass('ready');
                $("#file-upload img").hide();
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        } else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    });
    $('#update-cropp-image').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'base64',
            size: 'viewport'
        }).then(function (resp) {

            $("#img-preview").attr("src", resp);
            $("#img-preview").show();
            axios.post("/profile/avatar", {avatar: resp}).then(function (response) {
                location.reload();
            });

            $('#avatar-file').removeClass('ready');
            ev.stopPropagation();
            ev.preventDefault();
        });
    });
});