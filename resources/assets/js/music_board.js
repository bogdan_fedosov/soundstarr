var qq = require('fine-uploader/s3.fine-uploader/s3.fine-uploader');
$(function () {
    var uploaderAlbum = new qq.s3.FineUploader({
        element: document.getElementById("album-uploader"),
        request: {
            endpoint: 'test-ss2018.s3.amazonaws.com',
            accessKey: "AKIAJLIIQ7HMHBMUCGHA",
        },
        signature: {
            endpoint: '/s3/sign'
        },
        uploadSuccess: {
            endpoint: '/s3/success'
        },
        deleteFile: {
            enabled: true, // defaults to false
            endpoint: '/s3/delete'
        },
        retry: {
            enableAuto: true // defaults to false
        },
        callbacks: {

            onSubmitted: function (id, name) {
                var addon = "<div class=\"input-group-append\">\n" +
                    // "    <button class=\"btn btn-outline-secondary\" type=\"button\">Update</button>\n" +
                    "    <button class=\"btn btn-outline-secondary\ qq-upload-delete-selector qq-upload-delete\" type=\"button\">Remove</button>\n" +
                    "  </div>";
                $(".qq-file-id-" + id).prepend("<div class=\"input-group\ input-group-sm\"><input class='form-control' name='music-name-" + id + "' type='text' value='" + name + "'>" + addon + "</div>");
            },
            onProgress: function (id, name, uploadedBytes, totalBytes) {
                // ...
            },

            onComplete: function (id, name, responseJSON, onjRequest) {
                $("#form-popup-album").append("<input name='music-" + id + "' type='hidden' value='" + this.getKey(id) + "'>");
            }
        },
        validation: {
            allowedExtensions: ['mp3']

        }

    });

    var uploader = new qq.s3.FineUploader({
        element: document.getElementById("uploader"),
        request: {
            endpoint: 'test-ss2018.s3.amazonaws.com',
            accessKey: "AKIAJLIIQ7HMHBMUCGHA",
        },
        signature: {
            endpoint: '/s3/sign'
        },
        uploadSuccess: {
            endpoint: '/s3/success'
        },
        deleteFile: {
            enabled: true, // defaults to false
            endpoint: '/s3/delete'
        },
        retry: {
            enableAuto: true // defaults to false
        },
        callbacks: {

            onSubmitted: function (id, name) {
                var addon = "<div class=\"input-group-append\">\n" +
                    // "    <button class=\"btn btn-outline-secondary\" type=\"button\">Update</button>\n" +
                    "    <button class=\"btn btn-outline-secondary\ qq-upload-delete-selector qq-upload-delete\" type=\"button\">Remove</button>\n" +
                    "  </div>";
                $(".qq-file-id-" + id).prepend("<div class=\"input-group\ input-group-sm\"><input class='form-control' name='music-name-" + id + "' type='text' value='" + name + "'>" + addon + "</div>");
            },
            onProgress: function (id, name, uploadedBytes, totalBytes) {
                // ...
            },

            onComplete: function (id, name, responseJSON, onjRequest) {
                $("#form-popup-music").append("<input name='music-" + id + "' type='hidden' value='" + this.getKey(id) + "'>");
            }
        },
        validation: {
            allowedExtensions: ['mp3']

        }

    });
    $('#show-uploader').click(function () {
        $('.modal-footer, .uploader,.tags-wrap').show();
    });
//    $("#form-popup-music").submit(function (e) {
//
//        var data = $("#form-popup-music").serialize();
//        $.post("/music/store", data).done(function (data) {
//        })
//        e.preventDefault();
//
//    });
    $(".style-select").change(function () {
        var close = "<button type='button' class='close' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
        if (this.value != "") {
            $(".tag-list").append("<div class='tag'><input value='" + this.value + "' type='hidden' name='tag'>" + this.value + close + "</div>");
            $("#style-select").val("");
        }
    });

    $albumCrop = $('#avatar-file').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'square'
        },
        boundary: {
            width: 200,
            height: 200
        }
    });
    $("#upload-link").on('click', function (e) {
        e.preventDefault();
        $("#album-cover:hidden").trigger('click');
        $(".link-reload").hide();
        $(".btn-uploader").show();
    });

    $(".btn-uploader").click(function (ev) {
        $albumCrop.croppie('result', {
            type: 'base64',
            size: 'viewport'
        }).then(function (resp) {
            $('#image-cropped').val(resp);
            $(".album-img").attr("src", resp);
            $(".album-img").show();
            $(".btn-uploader").hide();
            $('#avatar-file').removeClass('ready');
            ev.stopPropagation();
            ev.preventDefault();
        });
    });
    $("#album-cover:hidden").change(function () {
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar-file').addClass('ready');
                $("img.album-img").hide();

                $albumCrop.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });

            };

            reader.readAsDataURL(input.files[0]);
        } else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    });

});
