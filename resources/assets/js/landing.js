
try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {
}

$(function () {
    $('#registerForm').submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        $.post($form.attr('action'), $form.serialize()).done(function (data) {
            location.reload();
        }).fail(function (response) {
            var errors = '';
            for (var i in response.responseJSON.errors) {
                var error = response.responseJSON.errors[i];
                if (!response.responseJSON.errors.hasOwnProperty(i)) {
                    continue;
                }
                errors += error.join('<br>') + '<br>';
            }
            $form.find('.errors-block').html(errors);
        });
    });
});
$(function () {
    $('#loginForm').submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        $.post($form.attr('action'), $form.serialize()).done(function (data) {
            location.reload();
        }).fail(function (response) {
            $form.find('.errors-block').html(response.responseJSON.errors.email[0]);
        });
    });
});

