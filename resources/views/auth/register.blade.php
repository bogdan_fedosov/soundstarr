<form id="registerForm" class="col-sm-10 offset-1" action="{{ route('register') }}" method="POST">
    @csrf

    <div class="errors-block"></div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" class="form-control" id="name" placeholder="Your name" name="name" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="email" class="form-control" id="email" placeholder="Your e-mail" name="email" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="password" class="form-control" id="password-confirm" name="password_confirmation" required
                   placeholder="Confirm password">
            </div>
        </div>
    </div>
</form>

@section('scripts')
    @parent
@endsection