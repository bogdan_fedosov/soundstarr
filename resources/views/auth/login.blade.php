<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="LoginModal">Log in</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body text-center">
                <form method="POST" action="{{ route('login', [], false) }}" id="loginForm">
                    @csrf

                    <div class="errors-block"></div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input id="email" type="email" class="form-control form-control-sm" name="email" required autofocus placeholder="E-Mail Address">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input id="password" type="password" class="form-control form-control-sm" name="password" required placeholder="Password">
                        </div>
                    </div>

                    <div class="form-check">
                        <input type="checkbox" name="remember" class="form-check-input" id="rememberMe">
                        <label class="form-check-label" for="rememberMe">Remember me</label>
                    </div>
                </form>
            </div>

            <div class="modal-footer modal-dialog-centered text-center">
                <button type="button" class="btn btn-secondary btn-sm mr-auto" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary btn-sm" form="loginForm">Login</button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    @parent
    <script src="/js/modals/login.js" type="text/javascript"></script>
@endsection
