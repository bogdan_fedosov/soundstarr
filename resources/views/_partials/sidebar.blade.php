
<nav  class="nav flex-column sidebar">
    <header class="navbar-text text-left text-dark font-weight-light">
        MENU
    </header>
    <li class="nav-item profile"><a class="nav-link {{ Request::is('profile/edit') ? 'active' : '' }}" href="{{route("profile-edit")}}">Edit profile</a></li>
    <li class="nav-item messages"><a class="nav-link " href="">Messages</a></li>
    <li class="nav-item news"><a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="/">News</a></li>
    <li class="nav-item notifications"><a class="nav-link" href="">Notifications</a></li>
    <li class="nav-item events"><a class="nav-link {{ Request::is('profile/events') ? 'active' : '' }}" href="{{route("profile-events")}}">Events</a></li>
    <li class="nav-item battles"><a class="nav-link" href="">Battles</a></li>
    <li class="nav-item music"><a class="nav-link {{ Request::is('profile/music-list') ? 'active' : '' }}" href="{{route("profile-music")}}">Saved playlist</a></li>
    <li class="nav-item video"><a class="nav-link {{ Request::is('profile/video-list') ? 'active' : '' }}" href="{{route("profile-video")}}">Saved video</a></li>
</nav>
<?php if(!empty($user->bio)): ?>
<nav  class="nav flex-column sidebar mt-4">
    <header class="navbar-text text-left text-dark font-weight-light">
        MORE INFO
    </header>

    {{$user->bio}}

</nav>
<?php endif; ?>
