
<nav  class="nav flex-column sidebar">
    <header class="navbar-text text-left text-dark font-weight-light">
        MENU
    </header>
    <li class="nav-item profile" data-toggle="modal" data-target="#exampleModalCenter"><a class="nav-link not-pressing" onclick="return 0"  href="">Feedback</a></li>
    <li class="nav-item messages"><a class="nav-link " href="">Send message</a></li>
    <li class="nav-item news"><a class="nav-link" href="/">Send offer</a></li>
    <li class="nav-item battles"><a class="nav-link" href="">Battles</a></li>
    <li class="nav-item music"><a class="nav-link " href="">Saved playlist</a></li>
    <li class="nav-item video"><a class="nav-link " href="">Saved video</a></li>
</nav>

<nav  class="nav flex-column sidebar mt-4">
    <header class="navbar-text text-left text-dark font-weight-light">
        MORE INFO
    </header>
    {{$user->bio}}
</nav>
