<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Soundstarr</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body class="content">
<header class="container">
    <!-- Just an image -->
    <nav class="navbar navbar-light bg-light navbar-expand-lg">

        <a class="navbar-brand" href="#">
            <div class="circle"></div>
            <h1 class="font-weight-bold">Soundstarr</h1>
        </a>
        <div class="collapse navbar-collapse d-flex justify-content-end " id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item mr-4 active">
                    <a class="nav-link" href="#">Dance <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item mr-4 ">
                    <a class="nav-link" href="#">Music</a>
                </li>
                <li class="nav-item mr-4">
                    <a class="nav-link" href="#">Charts</a>
                </li>
                <li class="nav-item mr-4">
                    <a class="nav-link" href="#">Battles</a>
                </li>
                <li class="nav-item mr-5">
                    <a class="nav-link" href="#">Party</a>
                </li>
                <li class="nav-item mr-3">

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <img src="{{$user->avatar}}" class="img-nav-ava">
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{route("profile-view")}}">My profile</a>
                        <form action="{{route("logout",[],false)}}" method="post" id='logout-link'>
                            @csrf
                            <a class="dropdown-item" href="#" onclick="document.forms['logout-link'].submit()">Log
                                out</a>
                        </form>
                    </div>
                </li>
                </li>
            </ul>
        </div>
    </nav>

</header>
<div class="container-fluid profile-description">
    <div class="container d-flex h-100">
        <div class="row pt-4 pb-4 pl-3">
            <div class="col-sm-3  justify-content-center align-self-center">
                <img src="{{$user->avatar}}">
            </div>
            <div class="col-sm-9 justify-content-center align-self-center pr-5">
                <header>
                    <h4 class="text-light">{{$user->name}}</h4>
                </header>
                <article class="row ">
                    <div class="col-sm-7 text-light page-heading">
                        <p>{{$user->page_heading}}</p>
                    </div>
                </article>
                <footer class="float-left">
                    <?php if(!empty($user->location)): ?>
                    <span class="location text-info ">{{$user->location}}</span>
                    <?php endif; ?>
                    <div class="clearfix mb-3"></div>
                    @if ($user->accept_offers)
                        <span class="availability text-success">Open to offers</span>
                    @else
                        <span class="availability text-danger">Currently busy</span>
                    @endif

                </footer>
                <div class="d-flex justify-content-end text-center ">
                    <div class="mr-2"><h3 class="clearfix text-danger font-weight-bold">8 </h3>
                        <p class="text-danger ">Following</p></div>
                    <div class="ml-4"><h3 class="clearfix text-danger font-weight-bold">87</h3>
                        <p class="text-danger">Followers</p></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mt-4">
    <div class="row">
        @yield('content')

    </div>
</div>
<footer id="footer" class="position-sticky">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h2>Soundstarr</h2>
                <div>&copy; Soundstarr 2018</div>
            </div>
            <div class="col-sm-9 text-right">
                <a href="/pages/about">About</a>
                <a href="/pages/advertising">Advertising</a>
                <a href="/pages/rules">Rules</a>
                <a href="/pages/about-wallet">About wallet</a>
                <a href="/pages/help">Help</a>
            </div>
        </div>
    </div>
</footer>
<script src="/js/app.js"></script>
@yield('scripts')


</body>
</html>