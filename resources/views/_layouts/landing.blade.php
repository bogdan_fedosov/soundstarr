<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Soundstarr</title>

        <link rel="stylesheet" href="/css/landing.css">
    </head>
    <body class="content container-fluid landing">
        <div class="presentation-block">
            <div class="container text-center">
                <div class="logo-wrapper">
                    <div class="logo-circle"></div>
                    <h1 class="logo-title">Soundstarr</h1>
                    
                </div>
                <h2>MUSIC IS OUR UNIVERSAL LANGUAGE!</h2>
                <p>Social network brings together young musicians and fans.</p>
                <div>
                    <div class="registration-wrapper">
                        <a href="#registration" class="btn btn-primary">Registration</a>
                    </div>
                    <div>
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#loginModal">Log in</button>
                    </div>
                </div>
            </div>
            <div class="presentation-block-overlay"></div>
        </div>

        <div class="create-account-block">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Create your account</h3>
                        <p>
                            Create a free account to join our music and entertainment online community.
                        </p>
                        <div class="registration-wrapper">
                            <a href="#registration" class="btn btn-primary">Registration</a>
                        </div>
                        <div class="presentation-img shadowed-2">
                            <img src="/img/landing/create-account-block.png" alt="" class="shadowed">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="capabilities-block">
            <div class="container text-center">
                <div class="row">
                    <div class="col-sm-3">
                        <div><img src="/img/icons/fun.png" alt="fun" class="icon"></div>
                        <h4>Fun</h4>
                        <p>
                            Discover new artists to follow. Share music with your friends. Find fun and new venues to attend in your area.
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/dancer.png" alt="dancer" class="icon"></div>
                        <h4>Dancer</h4>
                        <p>
                            Upload videos to your page. Showcase your talents and engage in dance battles!
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/artist.png" alt="artist" class="icon"></div>
                        <h4>Artist</h4>
                        <p>
                            Gain exposure by joining the largest social networking community exclusively for music and entertainment. 
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/organizer.png" alt="organizer" class="icon"></div>
                        <h4>Organizer</h4>
                        <p>
                            Venues can easily book new talent, sell tickets to their events and better promote their events using our 'Promotional Cubes'.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="manage-your-career">
            <div class="container">
                <div class="row">
                    <div class="offset-5 col-sm-7">
                        <h3>Manage your career</h3>
                        <p>
                             Customize your page to best represent your music. Allow your fans to follow you, purchase your tracks and merchandise. Make it easier for local venues to find and book you for events. Our platform offers a better and easier way for you to gain the most exposure.
                        </p>
                        <div>
                            <img src="/img/landing/create-board.png" alt="create board" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="capabilities-block second">
            <div class="container text-center">
                <div class="row">
                    <div class="col-sm-3">
                        <div><img src="/img/icons/wallet.png" alt="buy and sell" class="icon"></div>
                        <h4>Buy and sell</h4>
                        <p>
                            Purchase custom beats to create new tracks and sell your finished tracks to fans.
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/crowd.png" alt="engage in battles" class="icon"></div>
                        <h4>Engage in battles</h4>
                        <p>
                            Send or receive battles. Focus on winning to gain exposure, move up on our charts and win great prizes!
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/money.png" alt="sell merchandise" class="icon"></div>
                        <h4>Sell merchandise</h4>
                        <p>
                            Create custom merchandise to sell from your page. Allow fans to buy and support your brand. 
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/schedule.png" alt="create events" class="icon"></div>
                        <h4>Create events</h4>
                        <p>
                            Promote your upcoming events. Engage in our booking option to allow others to book you for events.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="love-music">
            <div class="container text-center">
                <div class="row">
                    <div class="offset-2 col-sm-8">
                        <div class="text-left">
                            <h3>If you love music</h3>
                            <p>
You can follow your favorite artists and purchase tickets to their events. Discover new talent by searching in specific genres, top trending artists or searching for new local artists in your area. Create, save and share custom playlists. Listen to or watch battles and vote for your favorite.                             </p>
                        </div>
                        <div class="registration-wrapper">
                            <a href="#registration" class="btn btn-primary">Registration</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="love-dance">
            <div class="container text-center">
                <div class="row">
                    <div class="offset-2 col-sm-8">
                        <div class="text-left">
                            <h3>If you love dance</h3>
                            <p>
                               Search different dance styles to view new and exciting videos. Whether you are a professional or recreational dancer, you can upload fun videos to showcase your talent!
                            </p>
                        </div>
                        <div class="registration-wrapper">
                            <a href="#registration" class="btn btn-primary">Registration</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="celebrate-block">
            <div class="container">
                <div class="row text-left">
                    <div class="col-sm-8">
                        <h3>If you are celebrating</h3>
                        <p>
                            Search any city to find local bars, clubs and venues. Get a listing by city or genre of music. View all venues' promotional cubes which will highlight their upcoming events, how many people will be attending and make it easier when deciding where to enjoy your evening. When you choose an event, click on the attending button to let your friends know where they can join you. Book your VIP table and taxi service to simplify your fun night out!
                        </p>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-sm-3">
                        <div><img src="/img/icons/star.svg" alt="top places" class="icon"></div>
                        <h4>Top places</h4>
                        <p>
                            Promote your venue get on top of
                            the best boost attendance
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/location.png" alt="add your venue" class="icon"></div>
                        <h4>Add your venue</h4>
                        <p>
                            Create your business page and promote
                            venues follow upcoming news
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/artist-2.png" alt="book artists" class="icon"></div>
                        <h4>Book artists</h4>
                        <p>
                            Hunt for cool dancers or musicians for
                            your event cooperate with the best
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div><img src="/img/icons/calendar.png" alt="upcoming events" class="icon"></div>
                        <h4>Upcoming Events</h4>
                        <p>
                            Learn about upcoming events Read
                            reviews choose the best parties!
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="registration-block">
            <div class="container text-center">
                <h3>start Registration</h3>
                <h5>Add to the creative community!</h5>
                <div class="row">
                    @include('auth.register')
                </div>
                <div>
                    <div class="registration-wrapper">
                        <button id="registration" type="submit" form="registerForm" class="btn btn-primary">Registration</button>
                    </div>
                    <div>
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#loginModal">Log in</button>
                    </div>
                </div>
            </div>
        </div>
        <footer id="footer" class="position-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h2>Soundstarr</h2>
                        <div>&copy; Soundstarr 2018</div>
                    </div>
                    <div class="col-sm-9 text-right">
                        <a href="/pages/about">About</a>
                        <a href="/pages/advertising">Advertising</a>
                        <a href="/pages/rules">Rules</a>
                        <a href="/pages/about-wallet">About wallet</a>
                        <a href="/pages/help">Help</a>
                    </div>
                </div>
            </div>
        </footer>

        @include('auth.login')
        <script src="/js/landing.js">
        @yield('scripts')
    </body>
</html>