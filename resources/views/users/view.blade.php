@extends('_layouts.main')
@section('content')
<div class="col-sm-3" id="sidebar">
    @include('_partials.sidebar')
</div>

<div class="col-sm-9 content main">


    <div class="row ">

        <div class="col-4 board board-create p-4 mt-3 mb-3">
            <a href="{{route("profile-boards-view")}}">
                <h5>Create board</h5>
                <div class="plus text-center">+</div>
                <p class="font-weight-light text-left">Load music, promotion and more</p>
            </a> 
        </div>
    </div>
    <div class="row">
        <header class="navbar-text text-left text-secondary col-12 text-uppercase">
            New added compositions
            <a href="{{route("profile-music")}}" class="float-right link-header">All added</a>
        </header>
    </div>
    <div class="row music p-4">
        <article class="music-details col-6">
            <a href="" class="play-it">
                <audio src="/test/Designer – Panda.mp3" >	
                </audio>
                <button class="play-button float-left mr-4"></button>
                <header> 
                    Designer-Panda(Prod by:Manance)
                </header>
            </a>
            <span>Into you</span>

        </article>
        <aside class="col-6 d-flex justify-content-end align-self-end music-actions">
            <p class="music-length">4:26</p>
            <div class="music-download text-blue mr-3">0</div>
            <div class="music-share text-blue mr-3">0</div>
            <div class="music-delete text-blue mr-3">Delete</div>
        </aside>
    </div>
    <div class="row music p-4">
        <article class="music-details col-6">
            <a href="" class="play-it">
                <audio src="/test/Designer – Panda.mp3" >	
                </audio>
                <button class="play-button float-left mr-4"></button>
                <header> 
                    Designer-Panda(Prod by:Manance)
                </header>
            </a>
            <span>Into you</span>

        </article>
        <aside class="col-6 d-flex justify-content-end align-self-end music-actions">
            <p class="music-length">4:26</p>
            <div class="music-download text-blue mr-3">0</div>
            <div class="music-share text-blue mr-3">0</div>
            <div class="music-delete text-blue mr-3">Delete</div>
        </aside>
    </div>
    <div class="row music p-4">
        <article class="music-details col-6">
            <a href="" class="play-it">
                <audio src="/test/Designer – Panda.mp3" >	
                </audio>
                <button class="play-button float-left mr-4"></button>
                <header> 
                    Designer-Panda(Prod by:Manance)
                </header>
            </a>
            <span>Into you</span>

        </article>
        <aside class="col-6 d-flex justify-content-end align-self-end music-actions">
            <p class="music-length">4:26</p>
            <div class="music-download text-blue mr-3">0</div>
            <div class="music-share text-blue mr-3">0</div>
            <div class="music-delete text-blue mr-3">Delete</div>
        </aside>
    </div>
    <div class="row">
        <header class="navbar-text text-left text-secondary col-12 text-uppercase">
            Feedback:
            <a href="" class="float-right link-header">All feedbacks</a>
        </header>
    </div>
    <div class="row">
        <div class="media p-2 col-12">
            <img class="mr-3 mt-2" src="/img/icons/ava2.png" alt="Generic placeholder image">
            <div class="media-body font-weight-light">
                <h5 class="mt-2">Katty Miller </h5>
                Excellent playlist. Well done and thank you so much ;)
            </div>
            <!-- <p class="review-time">24.01.1995</p> -->
        </div>
        <div class="media p-2 col-12">
            <img class="mr-3 mt-2" src="/img/icons/ava.png" alt="Generic placeholder image">
            <div class="media-body font-weight-light">
                <h5 class="mt-2">Katty Miller </h5>
                Excellent playlist. Well done and thank you so much ;)
            </div>
            <!-- <p class="review-time">24.01.1995</p> -->
        </div>
        <form method="post" novalidate class="needs-validation col-12 mt-4" action="">
            <textarea class="form-control" name="review" placeholder="Write feedback"></textarea>


            <button type="submit" class="btn btn-blue text-light mb-4 mt-2 float-right" >Send</button>
        </form>
    </div>

</div>
@endsection