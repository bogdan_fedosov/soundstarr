@extends('_layouts.main')
@section('content')
<div class="col-sm-3" id="sidebar">
    @include('_partials.sidebar')
</div>

<div class="col-sm-9 content main">

    <div class="row">
        <header class="navbar-text text-uppercase text-left text-secondary col-12">
            <div class="text-blue float-left pr-3 font-weight-bold"><</div>Profile
        </header>
    </div>
    <div class="row">
        <div class="board-select p-3 col-12">
            <ul class="list-group">
                <li class="list-group-item">Select theme board</li>
                <li class="list-group-item "><a href="{{route("profile-create-music")}}">Music</a></li>
                <li class="list-group-item"><a href="{{route("profile-create-video")}}">Video</a></li>
                <li class="list-group-item"><a href="{{route("profile-create-instituition")}}">My party instituition</a></li>
                <li class="list-group-item"><a href="">Merchandising</a></li>
                <li class="list-group-item"><a href="">Photos</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection

