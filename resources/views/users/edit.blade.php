@extends('_layouts.main')
@section('content')
    <div class="col-sm-3" id="sidebar">
        @include('_partials.sidebar')
    </div>

    <div class="col-sm-9 content main">

        <div class="row">
            <header class="navbar-text text-uppercase text-left text-secondary col-12">
                <div class="text-blue float-left pr-3 font-weight-bold"><</div>
                Edit Profile
            </header>
        </div>
        <div class="row mt-4">


            <form method="post" novalidate class="needs-validation col-12"
                  action="{{route("profile-update")}}">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="username">Name</label>
                        <input type="text" required class="form-control {{($errors->has('name'))?"is-invalid":""}}"
                               value="{{$user->name}}" id='username'
                               placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="name">
                        @if ($errors->has('name'))
                            <div class="invalid-feedback">
                                {{$errors->first('name')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email">Email</label>
                        <input type="email" required id='email'
                               class="form-control {{($errors->has('email'))?"is-invalid":""}}" value="{{$user->email}}"
                               placeholder="Email"
                               aria-label="Username" aria-describedby="basic-addon1" name="email">
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                {{$errors->first('email')}}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="bio">Short bio</label>
                        <textarea class="form-control {{($errors->has('bio'))?"is-invalid":""}}" id="bio" rows="3"
                                  name="bio">{{$user->bio}}</textarea>
                        @if ($errors->has('bio'))
                            <div class="invalid-feedback">
                                {{$errors->first('bio')}}
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="location">Location</label>

                        <input class="form-control {{($errors->has('location'))?"is-invalid":""}}" id="location"
                               name="location"
                               placeholder="Atlanta, Washington etc." value="{{$user->location}}">

                        @if ($errors->has('location'))
                            <div class="invalid-feedback">
                                {{$errors->first('location')}}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="page_heading">Page heading</label>
                    <textarea class="form-control  {{($errors->has('page_heading'))?"is-invalid":""}}" id="page_heading"
                              rows="3"
                              name="page_heading">{{$user->page_heading}}</textarea>
                    @if ($errors->has('page_heading'))
                        <div class="invalid-feedback">
                            {{$errors->first('page_heading')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <label>
                            <input class="form-check-input  {{($errors->has('accept_offers'))?"is-invalid":""}}"
                                   type="checkbox"
                                   <?php if ($user->accept_offers) echo "checked" ?> id="offer_check"
                                   name="accept_offers">
                            @if ($errors->has('accept_offers'))
                                <div class="invalid-feedback">
                                    {{$errors->first('accept_offers')}}
                                </div>
                            @endif
                            <span></span>
                            Accept offers
                        </label>

                    </div>
                </div>
                <button type="submit" class="btn btn-blue text-light mb-4">Update</button>
            </form>


        </div>
        <div class="row">
            <header class="navbar-text text-uppercase text-left text-secondary col-12">
                <div class="text-blue float-left pr-3 font-weight-bold"><</div>
                Your photo
            </header>
        </div>
        <div class="row mt-4">
            <header class="navbar-text text-left text-secondary col-12">

            </header>
            <form method="post" class="col-sm-6 float-left pt-2" enctype="multipart/form-data"
                  action="{{route("profile-edit-avatar")}}" id="file-upload">
                @csrf

                <div class="form-group text-center">
                    <img src="{{asset($user->avatar)}}" class="img-thumbnail" id="img-preview" alt="profile image">
                    <div id="avatar-file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-file">
                        <input class="form-control-file" type="file" name="avatar" id="file-selector">
                        <label class="custom-file-label" for="file-selector">Choose file</label>

                    </div>
                </div>
                <input type="submit" value="Update" id="update-cropp-image" class="btn btn-blue text-light mb-4">
            </form>

        </div>
    </div>
@endsection
@section('scripts')
    <script src='/js/edit_profile.js'></script>
@endsection