@extends('_layouts.main')
@section('content')
<div class="col-sm-3" id="sidebar">
    @include('_partials.sidebar')
</div>

<div class="col-sm-9 content main">

    <div class="row">
        <header class="navbar-text text-uppercase text-left text-secondary col-12">
            <div class="text-blue float-left pr-3 font-weight-bold"><</div>CREATE BOARD
        </header>
    </div>

    <div class="row board-page">
        <div class="board-select p-3 col-12">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
            <ul class="list-group">
                <li class="list-group-item">Music</li>
            </ul>
            <p class="mt-4">
                Loading your music here. You will be able to acquire new fans, get feedbacks as well as zarabotyvat money, allowing to 
                download your songs for commercial use. Simply enter the price compositions at loading.. Details <a href="#">here</a>
            </p>
        </div>
        <div class="col-12 d-flex justify-content-center mb-4">
            <button type="button" class="btn btn-blue text-white mr-2" data-toggle="modal" data-target="#albumModal">Create album</button>
            <button type="button" class="btn btn-outline-blue" data-toggle="modal" data-target="#musicModal">Load compositions</button>


        </div>
    </div>


    <div class="modal fade" id="musicModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">

            <div class="modal-content">
                <form id='form-popup-music' action="/music/store" method="POST"  class="needs-validation">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Create album</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row pt-4 pb-4 uploader">

                                <div id="uploader" class="col-12"></div>

                            </div>
                            <div class="row pt-4 pb-4 tags-wrap">
                                <div class="col-6">
                                    <input type="text" class="style-select form-control"
                                           list="list-styles1" id="style-select" placeholder="Add music style">
                                    <datalist id="list-styles1">
                                        <option value="Pop">
                                        <option value="Rock">
                                        <option value="Country">
                                    </datalist>
                                </div>
                                <div class="col-6 tag-list ">

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary text-uppercase" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-blue text-uppercase">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="albumModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">

            <div class="modal-content">
                <form id='form-popup-album' action="/album/store" method="POST"  class="needs-validation">
                     @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Create album</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row pb-4">
                                <div class="col-5">
                                    <div id="avatar-file"> 
                                    </div>
                                    <img src="http://via.placeholder.com/200x200" class="img-fluid album-img">
                                    <input type="hidden" value="" id="image-cropped" name="album.cover">
                                    <div class="col-12 link-reload pt-4">
                                        <input type="file" name="photo" id="album-cover" style="display:none">
                                        <a href="" id="upload-link">Reload photo</a>
                                    </div>
                                     <button type="button" class="ml-4 mt-4 btn-uploader btn btn-blue btn-sm" style="display:none">Update</button>
                                </div>
                               
                                <div class="col-7">

                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required name="album.name" placeholder="Album name">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="About album" required name="album.description" rows="5"></textarea>
                                    </div>
                                    <button type="button" id="show-uploader" class="btn btn-blue btn-lg btn-block text-uppercase">Load compositions</button>

                                </div>
                            </div>
                            <div class="row pt-4 pb-4 uploader" style="display:none">

                                <div id="album-uploader" class="col-12"></div>

                            </div>
                            <div class="row pt-4 pb-4 tags-wrap"  style="display:none">
                                <div class="col-6">
                                    <input type="text" class="style-select form-control" 
                                           list="list-styles2" id="style-select" placeholder="Add music style">
                                    <datalist id="list-styles2">
                                        <option value="Pop">
                                        <option value="Rock">
                                        <option value="Country">
                                    </datalist>
                                </div>
                                <div class="col-6 tag-list ">

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer" style="display:none">
                        <button type="button" class="btn btn-outline-primary text-uppercase" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-blue text-uppercase">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <script type="text/template" id="qq-template">
        @include('_partials/uploader')
    </script>

    @section('scripts')
    <script src='/js/music_board.js'></script>
    @endsection
</div>
@endsection