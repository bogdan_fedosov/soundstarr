@extends('_layouts.main')
@section('content')
<div class="col-sm-3" id="sidebar">
    @include('_partials.sidebar')
</div>

<div class="col-sm-9 content main">

    <div class="row">
        <header>

            <ul class="nav nav-tabs submenu">
                <li class="nav-item active">
                    <a class="nav-link " href="#">Saved videos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">My load videos</a>
                </li>

            </ul>
        </header>


    </div>

    <div class="row">
        <div class="col-6 p-4">
            <div class="embed-responsive embed-responsive-16by9">
                    <!-- <span class="video-title">Video name</span>
                            <span class="video-time">00:67</span>
                            <span class="video-description">Dance dance dance</span> -->
                <video class="embed-responsive-item video-js vjs-big-play-centered" id='vide' data-setup='{"controls":true}' src="https://www.w3schools.com/html/mov_bbb.mp4">

                    <source src="https://www.w3schools.com/html/mov_bbb.mp4" type='video/mp4'>

                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>


            </div>

        </div>
    </div>
</div>
@endsection