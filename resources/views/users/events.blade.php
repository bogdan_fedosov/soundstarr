@extends('_layouts.main')
@section('content')
<div class="col-sm-3" id="sidebar">
    @include('_partials.sidebar')
</div>

<div class="col-sm-9 content main">

    <div class="row">
        <header>

            <ul class="nav nav-tabs submenu">
                <li class="nav-item active">
                    <a class="nav-link " href="#">Want to visit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">My parties</a>
                </li>

            </ul>
        </header>


    </div>

    <div class="row mt-4">
        <div class="col-4 mb-4">
            <div class="card">
                <img class="card-img-top" src="http://unicornprinter.com/wp-content/uploads/2017/12/party-events-350x250.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold text-uppercase">Event title</h5>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                </div>
                <div class="card-footer-custom">

                    <p class="card-text font-weight-bold">23:00</p>

                    <div class="event-like text-blue">5644</div>
                    <div class="event-followers text-blue ">132</div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection