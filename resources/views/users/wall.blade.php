@extends('_layouts.main')

@section('content')
<div class="col-sm-3" id="sidebar">
    @include('_partials.sidebar')
</div>

<div class="col-sm-9 content main">
    <div class="row">
        <header>
            <ul class="nav nav-tabs submenu">
                <li class="nav-item">
                    <a class="nav-link active" href="#">All news</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Music</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Video</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Images</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Events</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Promotions</a>
                </li>
            </ul>
        </header>
    </div>
</div>




@endsection