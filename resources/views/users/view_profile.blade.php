@extends('_layouts.main')
@section('content')
<div class="col-sm-3" id="sidebar">
    @include('_partials.view-sidebar')
</div>

<div class="col-sm-9 content main">
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Send feedback</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <input type="hidden" name="posted_to" value="{{$user->id}}">
                    <div class="form-group col-md-12">
                        <textarea placeholder="Your feedback..." class="form-control" name="text" id="bio" rows="3"  name="bio"></textarea>
                        <button type="button" class="btn btn-blue float-right mt-4">Send</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection