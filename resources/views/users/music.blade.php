@extends('_layouts.main')
@section('content')
    <div class="col-sm-3" id="sidebar">
        @include('_partials.sidebar')
    </div>

    <div class="col-sm-9 content main">
        <div class="row">
            <header>
                <ul class="nav nav-tabs submenu">
                    <li class="nav-item active">
                        <a class="nav-link " href="#">Saved playlist</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">My loaded music</a>
                    </li>

                </ul>
            </header>
        </div>
        @forelse ($user->musics as $music)
            <div class="row music p-4">

                <article class="music-details col-6">
                    <a href="" class="play-it">
                        <audio src="{{$music->src}}">
                        </audio>
                        <button class="play-button float-left mr-4"></button>
                        <header>
                            {{$music->title}}
                        </header>
                    </a>
                    <?php if(isset($music->album->name)): ?>
                    <span> {{$music->album->name}}</span>
                    <?php endif; ?>
                </article>
                <aside class="col-6 d-flex justify-content-end align-self-end music-actions">
                    <p class="music-length">4:26</p>
                    <div class="music-download text-blue mr-3">0</div>
                    <div class="music-share text-blue mr-3">0</div>
                    <div class="music-delete text-blue mr-3">Delete</div>
                </aside>


            </div>
            @endforeach
    </div>
@endsection