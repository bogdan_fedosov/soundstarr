<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Route::get('/', function() {
    if (!Auth::guest()) {
        $user = Auth::user();
        return view("users.wall", ['user' => $user]);
    } else {
        return view("_layouts.landing");
    }
});

Route::get('/home', function() {
    return view("_layouts.landing");
});
Route::post('s3/sign',"BucketController@endpoint");
Route::post('s3/success',"BucketController@success");
Route::delete('s3/delete/{key}{bucket}',"BucketController@delete");

Route::post('music/store',"MusicController@store");
Route::post('album/store',"AlbumController@store");

Route::get('profile/music-list', "MusicController@list")->name("profile-music");

Route::get('profile/video-list', "UsersController@video")->name("profile-video");
Route::get('profile', "UsersController@view")->name("profile-view");
Route::get('profile/boards',"UsersController@boards")->name("profile-boards-view");
//Route::get('profile/{id}', "UsersController@viewProfile")->name("profile-view-other");
Route::get('profile/events', "UsersController@events")->name("profile-events");
Route::get('profile/boards/music', "UsersController@createMusicBoard")->name("profile-create-music");
Route::get('profile/boards/video', "UsersController@createVideoBoard")->name("profile-create-video");
Route::get('profile/boards/institution', "UsersController@createInstituitionBoard")->name("profile-create-instituition");

Route::get('profile/edit', "UsersController@edit")->name("profile-edit");
Route::post('profile/update', "UsersController@update")->name("profile-update");
Route::post('profile/avatar', "UsersController@avatar")->name("profile-edit-avatar");


Auth::routes();
