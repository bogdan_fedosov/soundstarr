<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\File;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{

    public function __construct()
    {

    }

    public function edit()
    {
        $user = Auth::user();
        return view("users.edit", ['user' => $user]);
    }

    public function update(Request $request)
    {

        $id = Auth::id();

        //validation
        $rules = array(
            'name' => 'required|max:190',
            'email' => 'required|email|max:190',
            'bio' => 'max:190',
            'page_heading' => 'max:190',
            'location' => 'max:190',
        );

        $validator = Validator::make($request->all(), $rules);
        // process update
        if ($validator->fails()) {
            $request->session()->flash('message', 'Error has occured!');
            $request->session()->flash('alert-class', 'alert-danger');
            return redirect()->route('profile-edit')
                ->withErrors($validator)
                ->withInput($request->except('password'));
        } else {
            // store
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->bio = $request->input('bio');
            $user->location = $request->input('location');
            $user->page_heading = $request->input('page_heading');
            $user->email = $request->input('email');
            $user->accept_offers = $request->has('accept_offers');
            $user->save();

            // redirect
            $request->session()->flash('message', 'Profile successfully updated!');
            $request->session()->flash('alert-class', 'alert-success');
            return redirect()->route('profile-edit');
        }
    }

    public function wall()
    {
        $user = Auth::user();
        return view("users.wall", ['user' => $user]);
    }

    public function avatar(Request $request)
    {
        $img = Image::make(file_get_contents($request->input("avatar")))->encode("png");

        $url = "avatars/" . uniqid() . ".png";
        Storage::disk('s3')->put($url, $img->__toString());
        $real_url = Storage::disk('s3')->url($url);

        $user = User::find(Auth::id());
        $user->avatar = $real_url;
        $user->save();
    }

    public function music()
    {
        $user = Auth::user();
        return view('users.music', ['user' => $user]);
    }

    public function video()
    {
        $user = Auth::user();
        return view('users.video', ['user' => $user]);
    }

    public function view()
    {
        $user = Auth::user();
        return view('users.view', ['user' => $user]);
    }

    public function boards()
    {
        $user = Auth::user();
        return view('users.boards', ['user' => $user]);
    }

    public function events()
    {
        $user = Auth::user();
        return view('users.events', ['user' => $user]);
    }

    public function createMusicBoard()
    {
        $user = Auth::user();
        return view('users.create_music_board', ['user' => $user]);
    }

    public function createVideoBoard()
    {
        return view('users.create_video_board');
    }

    public function createInstituitionBoard()
    {
        return view('users.create_instituition_board');
    }

    public function viewProfile($id)
    {
        $user = User::find($id);
        if (!$user) {
            throw new \Intervention\Image\Exception\NotFoundException();
        }
        return view('users.view_profile', ['user' => $user]);
    }

}
