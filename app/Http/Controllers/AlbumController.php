<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelMP3;
use Illuminate\Support\Facades\Auth;
use App\Album;
use App\Music;
use Illuminate\Support\Facades\Storage;

class AlbumController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->input();

        $album = new Album;
        $album->name = $data['album_name'];
        $album->saveCover($data['album_cover']);
        $album->description = $data['album_description'];
        $album->author_id = Auth::id();
        $album->save();

        for ($i = 0; ; $i++) {
            if (isset($data["music-$i"])) {
                $music = new Music;
                $music->title = $data["music-name-$i"];
                $url=  Storage::disk('s3')->url($data["music-$i"]);
                $music->src =$url;
                $music->album_id = $album->id;
                $music->author_id = Auth::id();
                $music->save();
            } else {
                break;
            }
        }

        $request->session()->flash('message', 'Album successfully created!');
        $request->session()->flash('alert-class', 'alert-success');
        return redirect()->route('profile-create-music');
    }
}
