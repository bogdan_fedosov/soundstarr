<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Album;
use App\Music;
use Illuminate\Support\Facades\Storage;


class MusicController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->input();

        for ($i = 0; ; $i++) {
            if (isset($data["music-$i"])) {
                $music = new Music;
                $music->title = $data["music-name-$i"];
                $music->src = Storage::disk('s3')->url($data["music-$i"]);
                $music->album_id = null;
                $music->author_id = Auth::id();
                $music->save();
            } else {
                break;
            }
        }

        $request->session()->flash('message', 'Music successfully added!');
        $request->session()->flash('alert-class', 'alert-success');
        return redirect()->route('profile-create-music');
    }


    public function list()
    {
        $user = Auth::user();

        return view('users.music', ['user' => $user]);
    }

}
