<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class Album extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function saveCover($coverData)
    {
        $img = Image::make(file_get_contents($coverData))->encode("png");

        $url = "albums/" . uniqid() . ".png";
        Storage::disk('s3')->put($url, $img->__toString());
        $real_url = Storage::disk('s3')->url($url);
        $this->cover = $real_url;
    }
}
