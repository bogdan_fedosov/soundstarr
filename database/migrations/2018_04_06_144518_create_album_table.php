<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('albums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('cover');
            $table->unsignedInteger('author_id');
            $table->timestamps();
        });
        Schema::table('albums',function(Blueprint $table){
             $table->foreign('author_id')
                    ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
         Schema::table('albums', function(Blueprint $table) {
            $table->dropForeign(['author_id']);
        });
        Schema::dropIfExists('albums');
    }

}
