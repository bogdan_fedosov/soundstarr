<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeedbacksAddForeignKeys extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('feedbacks', function(Blueprint $table) {
            $table->foreign('posted_by')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->foreign('posted_to')
                    ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('feedbacks', function(Blueprint $table) {
            $table->dropForeign('feedbacks_posted_by_foreign');
            $table->dropForeign('feedbacks_posted_to_foreign');
        });
    }

}
