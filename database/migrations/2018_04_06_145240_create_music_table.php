<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('musics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('src', 360);
            $table->unsignedInteger('album_id');
            $table->unsignedInteger('author_id');
            $table->timestamps();
        });

        Schema::table('musics', function(Blueprint $table) {
            $table->foreign('author_id')
                    ->references('id')->on('users');
            $table->foreign('album_id')
                    ->references('id')->on('albums');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('musics', function(Blueprint $table) {
            $table->dropForeign(['author_id','album_id']);
        });
        Schema::dropIfExists('music');
    }

}
